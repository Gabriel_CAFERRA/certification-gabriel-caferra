import sqlite3
import os
from pathlib import Path

def backup_creation(db_path,backup_repo, backup_file):

    con = sqlite3.connect(db_path)

    dump_filepath = Path(backup_repo) / Path(backup_file)
    with open(dump_filepath, 'w') as f:
        for line in con.iterdump():
            f.write('%s\n' % line)

    con.close()

    return dump_filepath