import requests
import json
import csv
import logging
from datetime import datetime

### Reference links 
### Horaires

def geojson_download(geojson_url, temp_dir: str, pol_name):

    now = datetime.now()
    current_time = now.strftime("%H:%M:%S")
    geojson_path = temp_dir / f"{pol_name}_{current_time}.geojson"

    logger.info(f"downloading {geojson_url}")
    r = requests.get(geojson_url, stream=True)

    logger.info(f"saving geojson at {geojson_path}")
    with open(geojson_path, "wb") as f:
        for chunk in r.iter_content(4096):
            f.write(chunk)

    return r