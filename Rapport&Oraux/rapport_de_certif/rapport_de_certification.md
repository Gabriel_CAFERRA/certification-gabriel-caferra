![front_page](./page_de_garde.png)
\pagebreak

# Table des matières

- [Introduction](#introduction)
  - [Contexte du projet](#contexte-du-projet)
  - [Qui est Atmo ?](#qui-est-atmo-)
- [Etude préparatoire](#etude-prparatoire)
  - [La compréhension du besoin client](#la-comprhension-du-besoin-client)
    - [Enoncé des besoins](#enonc-des-besoins)
    - [Comprendre les données atmos](#comprendre-les-donnes-atmos)
  - [Etat de l'art](#etat-de-lart)
    - [Le choix des étapes de pipeline](#le-choix-des-tapes-de-pipeline)
    - [La mise en place d'un pipeline](#la-mise-en-place-dun-pipeline)
    - [Les fichiers à retraiter](#les-fichiers--retraiter)
    - [Les moteurs de base de données](#les-moteurs-de-base-de-donnes)
    - [Les différents modèles de bases de données](#les-diffrents-modles-de-bases-de-donnes)
    - [Les différentes API de Geocoding](#les-diffrentes-api-de-geocoding)
    - [La visualisation des données](#la-visualisation-des-donnes)
- [Mise en place du projet](#mise-en-place-du-projet)
  - [La gestion de projet](#la-gestion-de-projet)
    - [Le premier entretien avec le client](#le-premier-entretien-avec-le-client)
    - [La définition du product backlog](#la-dfinition-du-product-backlog)
    - [Une première itération](#une-premire-itration)
    - [Le daily scrum](#le-daily-scrum)
    - [La fin d'un itération](#la-fin-dun-itration)
    - [Gitlab comme plateforme collaborative](#gitlab-comme-plateforme-collaborative)
  - [Le choix des technologies](#le-choix-des-technologies)
    - [Pour la mise en place du pipeline](#pour-la-mise-en-place-du-pipeline)
    - [L'extraction](#lextraction)
    - [Une première transformation](#une-premire-transformation)
    - [Premiers traitements de la donnée: la cohérence des données via les métadonnées](#premiers-traitements-de-la-donne-la-cohrence-des-donnes-via-les-mtadonnes)
  - [La conception de la base de données](#la-conception-de-la-base-de-donnes)
    - [Le choix de la base de données](#le-choix-de-la-base-de-donnes)
    - [Critères d'unicité](#critres-dunicit)
    - [L'enrichissement des données](#lenrichissement-des-donnes)
  - [Les visualisations](#les-visualisations)
    - [La cartographie](#la-cartographie)
    - [Les graphiques](#les-graphiques)
  - [La création de fichiers JSON](#la-cration-de-fichiers-json)
- [Retour d'expériences](#retour-dexpriences)
  - [Les difficultés rencontrées](#les-difficults-rencontres)
    - [la compréhension des fichiers disponibles](#la-comprhension-des-fichiers-disponibles)
    - [Concevoir la base de données](#concevoir-la-base-de-donnes)
    - [Concevoir l'application](#concevoir-lapplication)
    - [La manipulation des fichiers JSON et CSV](#la-manipulation-des-fichiers-json-et-csv)
  - [Ce qu'il resterait à faire](#ce-quil-resterait--faire)
    - [Mettre en place un environnement de test](#mettre-en-place-un-environnement-de-test)
    - [Une opportunité de tester la performance des contraintes d'unicité](#une-opportunit-de-tester-la-performance-des-contraintes-dunicit)
- [Références](#rfrences)

# Introduction

## Contexte du projet

Le 10 juillet 2020, le Conseil d'Etat a rendu publique une décision historique pour contraindre l'Etat Français à prendre des mesures immédiates en faveur de la qualité de l'air. Les seuils fixés par l'Europe pour les particules PM10 et le dioxyde d'azote NO2 doivent être respectés sous peine de lourdes amendes.

En Auvergne-Rhône-Alpes, différentes zones sont concernées par ces dépassements. La région a donc besoin de différents outils pour suivre et analyser la pollution atmosphérique à l'échelle de la région.

## Qui est Atmo ?

Le projet se base sur les relevés fournis par Atmo. Atmo Auvergne-Rhône-Alpes est l’observatoire agréé par le Ministère de la Transition écologique et solidaire, pour la surveillance et l’information sur la qualité de l’air en Auvergne-Rhône-Alpes. Elle est en charge de plusieurs missions d'intérêt général:

* Observer via un dispositif de surveillance chargé de la production, la bancarisation et la dissémination de données de référence sur la qualité de l’air
* Accompagner les décideurs dans l’élaboration et le suivi des plans d’actions à moyen et long terme sur l’air et les thématiques associées (énergie, climat, nuisances urbaines) comme en situations d’urgence (épisodes de pollution, incidents ou accidents industriels)
* Communiquer auprès des citoyens et les inviter à agir en faveur d’une amélioration de la qualité de l’air.
* Anticiper en prenant en compte les enjeux émergents de la pollution atmosphérique et les nouvelles technologies par la mise en place de partenariats dans le cadre d’expérimentations, d’innovations, de programmes européens
* Gérer la stratégie associative et l’animation territoriale, organiser les mutualisations en veillant à la la cohérence avec le niveau national.

L’observatoire de la qualité de l’air en Auvergne-Rhône-Alpes présente une grande diversité de métiers autours de ses principales activités : mesure de la qualité d’air, études et modélisation des phénomènes atmosphériques, analyse des données, cartographie, accompagnement des décideurs dans l’élaboration et le suivi des plans d’actions ou encore l’information du public.

# Etude préparatoire

## La compréhension du besoin client

### Enoncé des besoins

Atmo souhaite mettre à disposition de ses équipes:  

* une application facilement déployables et utilisable par l'équipe des techniciens
* alimentant une base de données embarquée générée à partir des fichiers GeoJSON via l'API déjà disponible
* l'application doit être légère car elle doit être exécutée toutes les heures sur des machines peu performantes

L'application doit permettre à l'équipe des techniciens de visualiser les différentes stations sur une carte interactive enrichies par:  

* des informations supplémentaires facilitant la localisation des stations, comme par exemple des compléments d'adresses.
* des indicateurs sur l'évolution de l'émission des différents polluants

Le but de l'application est de permettre aux techniciens de détecter facilement des anomalies dans le relevé des mesures, telles que des valeurs manquantes ou aberrantes.

### Comprendre les données atmos

L'organisme Atmo est en charge de la mesure des émissions de 5 catégories de polluants, le dioxyde d'azote (NO2), les particules fines (pm10 et pm25), l'ozone (O3) et le dioxyde de soufre (SO2).
Les données disponibles au téléchargement sont des relevés au format GeoJSON correspondants à chacun de ces polluants. Plusieurs versions de ces relevés sont disponibles.  

* Les relevés horaires:
Les concentrations moyennes horaires sont disponibles pour chaque heure, sur une période de 2 semaines.
* Les relevés journaliers:
Les concentrations moyennes journalières sont disponibles pour chaque jour sur une période de 1 an.
* Les relevés mensuelles:
Les concentration moyennes mensuelles sont disponibles pour chaque mois (premier jour du moins) sur une période de 1 an

Chaque enregistrement correspond aux mesures relevées dans les différentes stations de mesures de la pollution de l'air, à l'échelle de la région. Chaque station peut fournir des mesures pour différents polluants. Les données sont accessibles grâce à une API à l'adresse suivante : https://data-atmoaura.opendata.arcgis.com/

Remarques:  

* chaque relevé peut présenter des valeurs manquantes,
* les valeurs présentes dans les relevés ont parfois des valeurs négatives.


## Etat de l'art

Plusieurs aspects du besoin client sont à considérer d'un point de vue technique. Ces considérations entraîneront des choix techniques lors de la conception de l'application.

### Le choix des étapes de pipeline

### La mise en place d'un pipeline

#### En bash

L'une des options couramment retenues pour la conception d'un pipeline est d'utiliser le langage bash. Ce langage est directement intégré dans les environnements Linux et Unix, de ce fait:  

* il ne nécessite donc aucune installation supplémentaire,
* il est conçu pour intéragir facilement avec les utilitaires déjà présents sur le système d'exploitation.  

Ces avantages ont néanmoins des contreparties:

* il n'est pas portable sur les systèmes d'exploitation autres que ceux basés sur Unix/Linux et peut présenter des incompatibilités entre différentes distributions,
* ses fonctionnalités sont limitées.  

L'utilisation du langage bash est une solution robuste mais néanmoins limités en comparaison d'autres solutions comme Python.

#### En python

La réputation de Python est basée sur la diversité des libraires proposées. Elles permettent au langage Python de:

* réaliser la plupart des actions réalisables avec le langage Bash,
* proposer des fonctionnalités avancées, développées par une large communauté,
* fournir des outils de débogage avancés,
* créer des scripts plus facilement portables sur de nouveaux systèmes d'exploitation.

Les inconvénients principaux de Python sont:  

* une prise en main préalable est nécessaire,
* implique de concevoir une application là où bash peut se contenter d'éxécuter des commandes élémentaires,
* bien qu'un pipeline d'acquisition de données puisse être automatisé, l'automatisation de l'exécution du pipeline requiert des outils supplémentaires comme Cron ou un framework d'automatisation de workflow.  

A condiition de maîtriser la programmation en Python, ce langage permet de mettre rapidement en place des applications portables et évolutives mais il requiert des outils supplémentaires pour être intégré à un workflow.

#### Framework d'automatisation de workflow

Dans certains cas de pipelines élaborés, il peut être utile de faire appel à des framework d'automatisation de workflow. Ces frameworks permettent d'organiser des taĉhes et de les synchroniser. Ils peuvent dans certains cas se substituer à la création de scripts ou s'inclure en tant que compléments:  

* un framework comme Prefect, entièrement conçu en Python peut alors synchroniser des scripts ou des fonctions afin de planifier l’exécution d’un workflow,
* on peut intervenir sur les paramètres d'un workflow afin de le modifier, le faire évoluer,
* les frameworks sont adaptés à l'exécution en parallèle de plusieurs tâches et la gestion de leur interdépendance,
* les frameworks inclues des outils de monitoring là où les scripts fournissent moins ou aucune informations sur leur exécution.  

En tant qu'alternative ou outil complémentaire, les frameworks nécessitent:  

* de s'approprier un nouvel interface graphique et / ou, 
* de maîtriser des notions avancés des langages de programmation (comme les décorateurs Python)
Les frameworks sont particulièrement adaptés aux besoins des data scientists qui ont besoin d'outils d'orchestrations de tâches, tels que des reportings automatisés et des alertes.

### Les fichiers à retraiter

#### GeoJSON vs JSON

La différence fondamentale entre GeoJSON et JSON est leur niveau de standardisation:  

* JSON est un système de notation d'objet javascript. En ce sens, il constitue plus une spécification qu'un standard. Il représente un dictionnaire dans les clés peuvent elle-même contenir d'autre objet désigné à leur tour par de nouvelles clés.
* GeoJSON est un type de fichier qui reprend les spécifications des fichiers JSON mais ajoute certaines contraintes. Ainsi les enregistrements sont contenues dans un objet liste désigné par "features". Chaque élément la liste correspond donc à un enregistrement et chaque enregistrement contient deux sous-objets, "geometry" et "properties". L'objet "geometry" peut être de différents types, une ligne, un polygone, un point. L'objet "properties" contient l'ensemble des caractéristiques liées à l'objet "geometry", auxquelle aucune contrainte particulière n'est appliquée.


#### JSON vs CSV

Contrairement au format JSON, le format CSV ne présente aucune information de hiérarchie, c'est un simple fichier où les valeurs consécutives d'un enregistrement sont séparées par des virgules, chaque nouvel enregistrement étant marqué par un saut de ligne. L'absence de hiérarchie et d'objets imbriqués entraîne une perte d'information importante. Il faut veiller à ce que le formatage d'un CSV conserve les méta-données nécessaires, en premier-lieu l'ordre et la référence lors d'un enregistrement. La première valeur enregistrée doit toujours correspondre à une même caractéristique, la deuxième valeur correspond toujours à une deuxième caractéristique. Il faut ainsi s'assurer que la première ligne d'un csv est correspond à une en-tête qui référence chacune des valeurs d'un enregistrement.

#### Avec ou sans Pandas?

Pandas est une librairie très populaire chez les utilisateurs de Python, qui bénéficie de nombreuses fonctionnalités intégrées. Elle permet de manipuler des objets de type matriciels et de les parcourir de manières aisée. C'est un outil particulièrement apprécié des Data Scientists et des experts de Machine Learning. Cependant, dans le contexte d'un projet de développement de base de données, les fonctionnalités de Pandas n'offrent que peu d'intérêt et contribuent à complexifier le code.  

* Les tableaux manipulés correspondent à des dictionnaires ou des listes, déjà implémentés dans Python. Pandas n'offre qu'une réécriture du code avec une notation supplémentaire.
* L'une des fonctionnalités de Pandas permet d'éliminer les doublons présents dans un dataframe. On peut affiner le critère de duplicité en ne sélectionnant que certaines colonnes; si deux enregistrements partagent les mêmes valeurs sur ces colonnes, les deux enregistrements sont considérés comme doublons et on peut donc les éliminer. Cependant, cette méthode implique l'utilisation d'une librairie externe.  

Les fonctionnalités de Pandas sont reproductibles directement sans besoin d'étudier une nouvelle documentation.

### Les moteurs de base de données

#### SQLite

SQLite fait figure d'exception parmi les moteurs de base de données. Comme nous l'avons évoqué, la plupart d'entre eux reposent sur des serveurs sur lesquels on exécute des requêtes et l'on reçoit les réponses correspondantes. Une base de données SQLite se suffit à elle-même car le moteur est intégré au fichier contenant la base de données. Tout programme accédant à la base de données le fera par l'intermédiaire du moteur embarqué:

* on s'économise ainsi la configuration d'un serveur,
* le moteur SQLite est plus léger en terme de ressources et d'occupation que les moteurs client-serveur,
* la base de données est facilement portable. Il suffit de copier le fichier pour pouvoir le réutiliser sur une autre machine.  

Néanmoins, ces avantages ont les contreparties suivantes:

* La légèreté du moteur se fait au détriment des fonctionnalités avancés, notamment la programmation en langage SQL,
* Les possibilités de formatage des données sont limités à 5 types de données,
* L'accès simultané n'est pas prise en charge ce qui réduit son champ d'utilisation à des besoins spécifiques,
* L'accès à la base de données n'est pas sécurisée.

SQLite est donc adaptée à certains cas d'usage, comme par exemple des bases de données embarquées dans les applications de téléphonie mobile où la sécurisation se fait en amont de la base de données et les accès sont organisés pour ne pas être concurrents.

#### PostGRESQL

L'objectif de PostGRESQL est de proposer des fonctionnalités adaptées aux besoins actuels des entreprises. IL se base sur l'approche client-serveur. Le moteur PostGRES interroge la base de données disponibles sur un serveur pour fournir une réponse à son client.  

* PostgreSQL peut stocker plus de types de données que les types simples traditionnels utilisés dans SQLite,
* Plusieurs utilisateurs peuvent accéder simultanément à la base ce qui l'a rend adaptée aux grandes entreprises.
* L'accès à la base de données est sécurisée.  

Néanmoins,  

* Les fonctionnalités supplémentaires du moteur peuvent faire augmenter rapidement la taille de la base de données ainsi que les ressources nécessaires à son fonctionnement.
* L'accès sécurisé implique la nécessité d'administrer la base de données.

PostGRESQL est donc adapté à une utilisation en entreprise où les données sont "centralisées" dans un serveur. Dans ce cas de figure, plusieurs employés peuvent accéder à la base de données pour exécuter des requêtes, ou modifier la base de données.

![Modèles de bases de données](./SQLite_Architecture_French.png)

### Les différents modèles de bases de données 

#### Modèle transactionnel

Considérons le modèle transactionnel suivant:  

![Schéma transactionnel](./schema_transactionnel.png)

* Une station est forcément rattachée à une commune,
* Une commune est elle-même rattachée à un département,
Cette représentation est donc une modélisation de la réalité de l'acquisition de la donnée.

Le modèle transactionnel est adapté à l'acquisition de la donnée dès lors que l'intégrité est un aspect critique de la donnée. En reprenant notre exemple:

* Une station de comporter la référence au polluant,
* Un commune doit elle-même comporter une référence à un département,

Ainsi, on ne peut pas enregistrer une station sans avoir enregistré au préalable la commune à laquelle est rattachée la mesure. On s'assure ainsi de l'atomicité, celle-ci n'étant enregistrée que si elle est complète.
Le modèle transactionnel est donc avant tout orienté vers l'enregistrement sécurisé des données en bases. Néanmoins, lorsqu'il s'agit d'interroger la base de données, la tâche peut s'avérer complexe. Pour récupérer le département correspondant à un ensemble de station, il faudra:  

* d'abord interroger la table "commune" pour récupérer les références correspondantes puis,  
* récupérer les références de département à partir des références de communes.
Ce sont autant de données qu'il faut momentanément stocker en mémoire pour récupérer l'information voulue.
On voit donc que lorsqu'il s'agit de récupérer une information en base de données pour pouvoir l'exploiter, le modèle transactionnel alourdit les traitements.

#### Modèle analytique

![Modèle en étoile](./star_schema.png)

Ainsi, dès lors que la finalité d'une base de données est l'exploitation de ses informations à des fins d'analyse, il est préférable d'opter pour le modèle analytique ou modèle en étoile.   

Le modèle en étoile permet d'organiser l'ensemble des table autour d'une centrale, la table des "faits" qui correspond aux observations que l'on souhaite analyser. Ainsi la table centrale est caractérisée par un ensemble de dimensions représentées par le reste des tables. En reprenant le schéma de l'annexe, le fait analysé correspond à une vente. Une vente est caractérisée par plusieurs dimensions: le produit acheté (table "product"), le client qui a réalisé l'achat (table "client"), le lieu de la commande (table "zipcode"), le mode de livraison (table "shipping_methode"). Ainsi la table centrale contient une référence à la table dimension correspondante, et ceci pour chaque dimension. Dans l'exemple de l'annexe, la valeur "1" de ID_client dans la table centrale fait référence à la valeur "1" de ID dans la table "client". Cette ID est lié à un "first_name" et un "last_name" qui caractérisent un client.  
L'avantage du modèle analytique est de limiter les étapes intermédiaires pour récupérer l'information contenue dans une table contrairement au modèle transactionnel. L'écriture des requêtes est alors grandement simplifiée, d'autant plus si la base de données contient de nombreuses tables.

#### Les relations many-to-many and one-to-one


Considérons le cas où l'on veuille affecter l'équipe des techniciens à l'ensemble des stations:  

* un technicien peut être affecté à plusieurs stations,
* une station peut être maintenue par plusieurs techniciens,  

Ces deux tables ont donc une relation many-to-many, il faut ajouter un champ dans chaque table, contenant:

* pour une station, une liste d'identifiant de technicien,
* pour un technicien, une liste d'identifiant de station,

Ce système complique la mise à jour de la base de données et demande un retraitement lors du requêtage sur ce nouveau champ. La solution est de créer une table d'association selon le modèle suivant.

![Table d'association](./association.png)

La table d'association contient 2 champs où figurent l'ensemble des combinaisons possibles de stations et de techniciens. Chacun de ces champs est la clé étrangère qui fait référence aux identifiants présents dans les 2 tables. 

### Les différentes API de Geocoding

Une API est une interface disposant d'un ensemble de règles par laquelle elle communique et échange d'autres logiciels. Elle est offerte par une bibliothèque logicielle ou un service web. Un utilisateur peut alors interagir avec cette API par l'intermédiaire d'un logiciel en respectant l'ensemble des règles définies par l'API.

#### Nominatim

Nominatim repose sur la librairie Geocoder et OpenStreetMap. Comme ce package utilise des services mis à disposition gratuitement il n’est pas fait pour faire du géocodage en masse et des délais sont rajoutés entre les requêtes pour ne pas surcharger les serveurs. Il existe néanmoins des adaptations permettant de contourner ses limitations. 
Elle offre la possibilité d'enrichir les données de géolocalisation, dans les "2 sens":

* On peut récupérer les latitudes et longitudes à partir de données textuelles, 
* Récupérer l'adresse à partir des coordonnées,

Dans ce dernier cas, l'objet retourné est un dictionnaire dont le noms des clés peut varier, selon les coordonnées qui ont été rentrées. Par exemple, si les coordonnées pointent vers un hameau, la clé "hamlet" peut figurer dans le dictionnaire renvoyé. De même, si les coordonnées pointent en plein centre-ville d'une ville importante, la clé "district" correspondant à l'arrondissement apparaîtra.
Nominatim pose donc la question de la standardisation du format des données.

#### L'API du gouvernement

Une alternative à Nominatim est l'API du gouvernement, un service de géocodage gratuit mis à disposition par Etalab. L'avantage de par rapport à Nominatim est l'utilisation de la spécification GeoCodeJSON qui offre une standardisation du dictionnaire renvoyé, quelque soit les coordonnées entrées. Cependant, cette API présente plusieurs inconvénients:

* Les informations récupérées se limitent à la France,
* L'API ne bénéficie pas d'une intégration à Python et de ce fait, la récupération des données nécessitent de s'approprier une syntaxe spécifique. Voici un exemple de geocoding à partir de coordonnées:

```bash
      curl "https://api-adresse.data.gouv.fr/reverse/?lon=2.37&lat=48.357&type=street"
```

#### L'API Geocoding de Google

L'utilisation de l'API Geocoding de Google est limitée. Dans le cas  d'une utilisation gratuite, vous pouvez effectuer jusqu'à 2500 requêtes par tranche de 24h. Si vous disposez d'un compte Google Maps API for Business, vous pouvez en effectuer jusqu'à 100000 par tranche de 24h. les requêtes les résultats du géocodage doivent obligatoirement être utilisés sur une Google map. Outre l'absence d'utilisation libre des données issues du geocoding et le rationnement du nombre de requêtes, l'API de Google ne bénéficie pas d'une réelle intégration à l'environnement Python. 

### La visualisation des données

#### La cartographie

##### GeoPandas

GeoPandas est une librairie de cartographie qui permet de générer rapidement des cartographies, elle présente:  

* une prise en charge de nombreux formats de fichiers de géolocalisation comme GeoJSON
* des fonctionnalités pensées pour la datascience avec l'intégration des DataFrame pandas.
De ce fait, les atouts de GeoPandas entraînent également:
* de fortes dépendances aux autres librairies de datascience, leur installation est nécessaire pour bénéficier de GeoPandas
GeoPandas trouve donc un intérêt dans une utilisation orientée datascience.

##### Folium

Folium est une librairie Python qui permet de générer des cartes interactives en s'appuyant la librairie Leaflet.js.  

* sa prise en main est intuitive, la construction d'une carte fait appel à des méthodes qui génèrent les éléments visualisables sur la carte,
* des fonctionnalités pensées pour une visualisation claire et interactive facilitant la lecture des cartes, comme les clusters de marker ou les choropleth,
* les cartes générées sont directement visualisables dans un navigateur web,
* Folium n'implique aucune dépendance aux autres librairies de data science telles que pandas, numpy ou mathplotlib
Néanmoins, pour une utilisation directe des fichiers de géolocalisation tel que GeoJSON:
* l'utilisation de Folium peut nécessiter des traitements préalables comme la transformation du fichier ou l'utilisation d'autres librairies telles que Pandas.
Folium représente donc un choix judicieux lorsque l'on ne souhaite pas utiliser d'autres librairies tels que Pandas.

#### Les graphiques

##### Matplotlib

Matplotlib est une librairie de visualisation très populaire auprès de la communauté scientifique:  

* elle est conçu pour le calcul scientifique et ses représentations graphiques,
* elle est conçu pour être le complément graphique des autres librairies scientifiques comme numpy  

De ce fait:

* Matplotlib est dépendante de la librairie numpy,
* si la création de graphiques statiques est accessible avec Matplotlib, les graphiques intéractifs nécessitent des modules complémentaires qui demandent des manipulations spécifiques.
Matplotlib est donc une librairie adaptée pour créer des graphiques scientifiques complexes mais dont l'utilisation s'avère complexe.

##### Plotly

Plotly est une librairie qui gagne en popularité dans la communauté Python. A l'instar de Folium, il s'agit d'une librairie Python permettant d'interagir avec une librairie javascript. De ce fait, ses intérêts sont:  

* un large choix de types de représentations différentes parfois uniques à cette librairie,
* une prise en main rapide,
* des présentations interactives directement visualisables dans le navigateur que l'on peut écrire dans un fichier HTML.
Jusqu'à la version 4, par défaut, Plotly interagissait avec ses propres serveurs:
* ce qui rend la génération des graphiques dépendantes à la qualité de la connexion,
* la facilité de prise en main entraîne des limites à la personnalisation des représentations graphiques,
Plotly est donc un outil facile d'accès pour générer des graphiques interactifs visualisables sur un navigateur.

#### Les plateformes dédiés, l'exemple de Metabase

La dernière approche consisterait à utiliser une plateforme de visualisation orientée Business Intelligence comme Metabase:  

* c'est une solution open source, en ligne mais également téléchargeable,
* s'adapte aux bases de données embarquées mais aussi client-serveur,
* elle intègre nombreux types de visualisations, des graphiques ainsi que cartographies,
* elle permet l'insertion de requêtes SQL et de générer les visualisations correspondantes.
Néanmoins, cela crée dépendance à la plateforme et ses contraintes:
* l'accès à la plateforme en ligne ne peut se faire de manière automatique depuis Python et demande une configuration supplémentaire,
même pour la version embarquée,
* il est impossible de générer un ensemble de fichiers à la volée de manière automatique,
Metabase est un outil qui permet un gain de temps pour des visualisations ponctuelles mais n'offre pas l'automatisation nécessaire pour un grand nombre de visualisations.

# Mise en place du projet

## La gestion de projet

Au sein de notre équipe composé de 2 personnes nous avons décidé de mettre en place la méthode Agile. Ce choix est justifié par le fait que notre client était une équipe non technique. La méthodologie Agile s'adapte particulièrement à ce cas de figure car il permet de définir un ensemble d'objectifs proposés par les concepteurs et validés par le client. Mon binôme remplissait le rôle de product owner tandis que j'occupais le rôle de Scrum master.

### Le premier entretien avec le client

Lors d'un premier entretien, le product owner a défini avec le client l'objectif initial du projet:  

* obtenir une application permettant de consulter heure par heure les relevés de pollution pour chacune des stations de la région, 
* les exigences étaient d'avoir "une application légère, une cartographie des stations ainsi qu'un historique des relevés".

### La définition du product backlog

Suite à cet entretien, le product owner a identifié une série de besoins utilisateurs (user stories) indépendants les uns des autres. L'ensemble de ces besoins constituent le product backlog. Chaque user story bénéficie d'une priorité évaluée avec le client.

### Une première itération

La méthode Scrum s'articule autour d'un ensemble d'itération, désignées sous le nom de sprints. Chaque sprint se décompose en plusieurs étapes marquées par des cérémonies, présidées par le Scrum Master.
Lors du sprint planning, chaque user story est traduite en terme technique, chacune de ces tâches techniques est généralement appelée "issue".  

* Ces issues sont évaluées lors du poker planning où chaque membre de l'équipe donne une estimation de la difficulté à réaliser la tâche.
* En prenant en compte la priorité du besoin utilisateur à laquelle elle est rattaché, et sa difficulté, on sélectionne un ensemble d'issue. Cet ensemble de tâches à accomplir constituent le sprint backlog.
L'objectif du sprint est de fournir au client un premier livrable.

### Le daily scrum

Le daily scrum est un échange quotidien où chacun fait part de son avancement et des difficultés rencontrées. C'est l'occasion d'aménager l'organisation de l'équipe dans la réalisation de chaque tâche technique.

### La fin d'un itération

A l'issue du délai imparti pour un sprint, on organise la sprint review, présidée par le product owner:  

* pour présenter l'avancement du projet au client, pour chaque fonctionnalité mise en place,
* c'est l'occasion pour le product owner de clarifier le besoin utilisation et d'adapter le product backlog le cas échéant.  

A l'issue de la sprint review, l'équipe scrum se réunit pour la sprint retrospective présidée par le scrum master:  

* c'est l'occasion pour l'ensemble de l'équipe de faire le bilan du sprint, afin d'identifier les axes d'amélioration. L'équipe peut s'appuyer sur le burndown chart et le scrum board, dont voici un exemple:  

![Exemple de scrum board](scrum_board.png)

La fin de cette cérémonie marque également la fin du sprint. On passe alors au sprint suivant en reprenant les mêmes étapes énoncés ci-dessus.

### Gitlab comme plateforme collaborative

Nous avons utiliser Gitlab comme dépôt de sauvegarde mais également dans le processus Agile:  

* la décomposition des user stories en tâches techniques donne lieu à la création d'une issue qui peut être assignée à un développeur.
* on peut alors suivre l'avancement du sprint en consultant le scrum board

## Le choix des technologies

En accord avec les points cités dans la partie état de l'art nous avons choisi d'utiliser Python et SQLite. Ce choix est motivé par le besoin de créer une application simple à déployer et à utiliser, qui nécessite le moins de manipulation de la part de l'utilisateur:
une fois installée, l'application de charge de tous les aspects du pipeline de la données avec l'aide du langage SQLite. Dans les prochaines sections, nous détaillerons l'emploi de Python et SQLite pour justifier notre choix technique.  

### Pour la mise en place du pipeline

Les données disponibles proviennent de l'API mise à disposition par le client. Il s'agira donc dans un premier temps de mettre en place la procédure d'extraction. 
Pour procéder à l'extraction, nous avons privilégié l'utilisation de Python plutôt que de passer par des commandes du terminal. Ce choix est motivé par les fonctionnalités de Python qui nous ont permis:

* un contrôle avancé de la procédure de téléchargement, 
* la conversion à la volée des fichiers et le stockage des fichiers convertis. 

### L'extraction 

Si les relevés journaliers et mensuels sont disponibles, le téléchargement des données se fera toutes les heures. Le besoin du client étant d'avoir un suivi heure par heure, il faut éviter la redondance des données. La possibilité de recalculer les valeurs journalières et mensuelles restent possible pour une analyse sur plus long terme.  

* La procédure de téléchargement se fait par l'intermédiaire de la librairie requests de Python, et l'utilisation de la méthode requests.get qui prend en argument l'url vers les fichiers de téléchargement. La valeur renvoyée est un objet de type response. Cet objet de type response contient un version texte du dictionnaire GeoJSON téléchargé.


### Une première transformation

Python est capable de convertir à la volée cet objet de type response en dictionnaire json grâce à la méthode .json(). Le dictionnaire obtenu n'est pas stockée en dur en tant que fichier de sauvegarde. Ce choix est motivé par le fait que:  

* les fichiers GeoJSON présentent une redondance avec les fichiers qui seront effectivement exploités pour la base de données,
* de fait, nous préférerons garder les fichiers qui seront effectivement exploités par la base pour constituer notre banque de sauvegardes, 
* les fichiers exploités seront des fichiers plats au format .csv que l'on pourra ré-exploiter plus facilement dans d'autres applications par rapport aux fichiers structurés de type .json ou .geojson, 
* le format .csv s'adapte particulièrement bien à l'importation avec SQLite,
* les fichiers .csv sont également simples d'utilisation dans d'autres logiciels comme les tableurs.  

Les fichiers temporaires GeoJSON utilisés sont parcourus sur la clé "features" qui contient chacun des enregistrements.  

* Sur chacun des enregistrements nous ne garderons que le contenu de la clé "properties". 
* Chacune des sous-clés correspond aux noms des entêtes du fichier .csv. 
Nous avons donc récupérer le contenu des clés pour écrire les lignes successives du fichier csv.  

Nous obtenons un fichier csv par fichier GeoJSON, correspondant à un relevé horaire pour chacun des polluants que le client souhaite suivre heure après heure. Pour faciliter la gestion de ces fichiers de sauvegardes et leur exploitation ultérieures, la nomenclature retenue est "<nom_du_polluant>_<jour_mois_année>_<heure_minute_seconde>.csv" avec pour référentiel le moment du téléchargement. Pour ce faire, nous avons utilisé la librairie datetime et les méthodes associées.

### Premiers traitements de la donnée: la cohérence des données via les métadonnées

A l'issue de cette étape, un premier contrôle a été réalisés sur la cohérence des données. Bien qu'on puisse supposer que le fichier GeoJSON soit généré de manière automatique et que son formatage soit correct, on ne peut pas certifier la validité du fichier téléchargé:  

* Une manipulation manuelle peut avoir été faite sur le fichier. Si aucun parser ne vérifie automatiquement la validité du fichier JSON selon un schéma prédéfini, le traitement du fichier entraînera une erreur et potentiellement une perte d'information significative. A titre d'exemple, changer le nom de la clé du sous-objet "properties" sur un enregistrement ne change pas la conformité du fichier avec le format JSON.  
Pour garder la cohérence des données en préservant l'information, on choisit de vérifie le nom des clés des objets "properties" de chaque enregistrement (les objets de "features"). Si la vérification n'aboutit pas, un message d'avertissement est envoyé et l'objet incriminé n'est pas enregistré dans le fichier csv. On passe simplement à l'objet suivant.

Bien qu'on puisse supposer que les fichiers CSV sont générés puis directement chargés en base. Aucune limitation n'est imposée pour charger des fichiers supplémentaires provenant d'autres sources. On effectue alors un nouveau contrôle sur les métadonnées des fichiers CSV en vérifiant que l'entête du fichier correspondent bien aux champs spécifiés dans la table des données brutes de la base de données.

On peut également effectué un contrôle sur la qualité des données:

* Disposer d'un jeu de données strictement identique pour des objets différents provenant des fichiers JSON n'apportera aucune information supplémentaire mais entraîne une augmentation du besoin du stockage, potentiellement problématique. Il est alors possible de ne pas enregistrer des objets JSON qui présentent les mêmes valeurs sur l'ensemble des clés du sous-objet "properties". on peut créer une liste de dictionnaire, chacun des dictionnaires correspondant à une ligne du fichier csv. A chaque nouvelle ligne traitée, on vérifie que le dictionnaire correspondant n'est pas présent dans la liste des dictionnaires.
* Cependant, cette approche a le désavantage de créer une copie des lignes des fichiers sous la forme de dictionnaire ce qui demande plus de ressources lors de l'exécution du programme. De plus, chaque ligne doit être comparée avec les éléments déjà présent dans le dictionnaire augmentant ainsi le temps de traitement.

Le contrôle sur la qualité des données n'a pas été retenue pour l'heure car le temps d'exécution du programme est grandement impactée. De plus, elle présente un intérêt limité par rapport à une solution plus robuste lors du chargement des fichiers en base de données. Comme nous le verrons par la suite.
Le but des fichiers csv est de retranscrire au mieux l'état initial d'acquisition de la donnée en dépit de la redondance potentielle, tout en maintenant la cohérence.

## La conception de la base de données

Dans une volonté de simplification du déploiement et de portabilité, nous avons porté notre choix sur une base de données SLQlite. Bien que PostGRES présente des fonctionnalités supplémentaires par rapport à SQLite, cette technologie implique une configuration supplémentaire avec une connexion à un serveur distant, ou local sous la forme d'un conteneur. Il faut alors passer par des commandes bash pour réaliser l'installation. Python et SQLite permettent d'éviter cet étape.

### Le choix de la base de données

Du fait que l'acquisition et l'enregistrement initial des mesures est déjà effectué. Le choix d'une base de données de type transactionnelle n'est pas justifiée:  

* Si une mesure n'est pas rattachée à un polluant ou une station, la mise en place d'un modèle transactionnelle assurerait l'atomicité et l'intégrité des données mais supprimerait potentiellement des informations.
* L'atomicité de la donnée n'est pas une nécessité dans la conception de notre application, au contraire, le fait qu'une valeur dans un champ soit manquante constitue une information dans notre analyse. Elle peut indiquer un dysfonctionnement lors de l'acquisition de la donnée par la sonde, un dysfonctionnement ou un formatage incorrect lors du transfert de l'enregistrement par la station.

Le modèle transactionnel n'est donc pas adapté puisqu'il s'agit d'analyser les données reçues par les stations.

Nous opterons donc pour un modèle analytique puisque notre application se concentre sur l'analyse des résultats.  

* Ainsi si une valeur n'est pas rattachée à une station, elle figurera quand même parmi les enregistrements de la table centrale, mais le champ correspondant à la référence de la station sera vide. 
* Le reste de l'enregistrement sera préservé.

Dans le modèle ci-dessous:  

![Modèle analytique retenu pour le projet](./ModèleAnalytique2.png)

* la table "mesure" est la table centrale qui dispose d'une clé primaire, indiquée par le symbole *,
* autour de laquelle s'articule les tables de dimensions, 
* chaque enregistrement dispose d'une clé de référence à la valeur correspondante dans une des tables de dimensions. Cette clé de référence est appelée clé étrangère, elle est indiquée par le symbole **,
* chaque enregistrement de la table mesure doit contenir obligatoirement une valeur pour la clé étrangère, 
* la valeur de la clé étrangère doit être égale à une valeur de clé primaire dans la table de dimension correspondante,
* par exemple, sur la table "station", la notation 1,1 signifie que 1 mesure ne peut correspondre qu'à une seule référence de station,
* de même, sur la table "mesure", la notation 1,N signifie que 1 station peut être référencée sur N mesures (plusieurs mesures).

Le fait "d'éclater" les tables de dimensions permet:  

* de limiter la redondance des données. Plusieurs communes sont situées dans le même département. Si l'on regroupe le champ "département" dans la table "commune", une même valeur de commune se retrouvera dans plusieurs enregistrements.
Eclater les tables permet donc indexer les champs de la table clean_data lors de la création des tables de dimensions:
* les requêtes d'analyse sont alors plus performantes en se limitant à une jointure pour chacune des dimensions étudiées, sans ordre à respecter puisque les tables de dimensions ne dépendent pas les unes des autres. Voici un exemple de requête illustrant notre propos:

```sqlite
      SELECT SUM(valeur = '') as val_manq, SUM(valeur < 0) as val_neg
        FROM mesure
        JOIN polluant on polluant."id_polluant" = mesure."reference_polluant"
        JOIN station on station."id_station" = mesure."reference_station"
        AND datetime(mesure."date_debut") <= datetime('now') 
        AND datetime(mesure."date_debut") >= datetime('now', '-1 month')
        WHERE station."nom_station" = ? AND polluant."id_polluant" = ?;
```

* on évite l'enchaînement ordonné des jointures entre les tables. Ces jointures successives doivent être réalisées dans des requêtes imbriquées, obligeant le moteur de base de données à garder en mémoire le résultat de la requête précédente pour exécuter la suivante.


### Critères d'unicité

Sachant que les relevés horaires couvrent une période de deux semaines, le téléchargement toutes les heures entraînera une duplication des données importantes. Il est donc important de gérer les doublons au sein de la base de données.

#### Consulter le fichier des métadonnées

Lors du téléchargement des fichiers GeoJSON, nous avons créer un fichier JSON qui enregistre les données des fichiers téléchargés. Chaque fichier téléchargé, donne lieu à un nouvel enregistrement sous la forme d'un dictionnaire:  

* la première clé contient le nom du polluant,
* la deuxième clé contient la date du téléchargement,
* la troisième clé contient l'heure du téléchargement (sans les minutes et les secondes),
* les autre clés contiennent le nombre de lignes, le nom des colonnes et l'url de téléchargement,
Ainsi avant de télécharger un fichier on doit s'assurer que le polluant ainsi que la date et l'heure ne figure pas déjà dans le fichier JSON.

#### Lors du chargement en base de données

##### Sur la table clean data

Une deuxième manière d'éviter de copier les mêmes informations en base de données et de créer une table "tampon" dans la base de données. Ainsi chaque fichier csv téléchargé en base l'est d'abord dans la table raw_data:  

* à chaque téléchargement et copie en base dans la table raw_data, on obtient potentiellement des doublons. C'est le cas si les enregistrements sont identiques au sein de fichiers csv différents. Nous n'avons aucune garantie sur le contenu de chaque fichier csv.
* une fois copié dans la table raw_data, les enregistrement ne sont copiés dans clean data que s'ils respectent les contraintes d'unicités de la table clean_data. Voici un extrait du fichier "structure.sql" qui entraîne la création de chaque table:

```sqlite
CREATE TABLE IF NOT EXISTS clean_data (
      ...
UNIQUE (nom_station, nom_poll, valeur, date_debut, date_fin, x_wgs84, y_wgs84)
```

Après une enquête métier auprès des techniciens, nous avons déterminé qu'un enregistrement est unique s'il s'agit de combinaisons différentes de valeurs pour les sept champs ci-dessus. Autrement dit dès lors que deux enregistrements partagent des valeurs identiques sur l'ensemble des champs sélectionnés, alors on considère qu'il s'agit de la même information. On ne garde donc pas la valeur inutile dans clean_data.

Remarques:

* On aurait pu appliquer une première contrainte sur la table raw_data pour ne garder que les enregistrements uniques sur tout les champs mais la table raw_data est nettoyée de tout ses enregistrements à chaque importation de série de fichiers. De plus, le critère d'unicité est plus restrictif sur la table clean_data et les enregistrements identiques sont forcément supprimés. 
* Il s'agit également d'une contrainte technique lié à l'interaction entre Python et SQLite. Il n'est pas possible pour Python d'insérer une liste d'enregistrements dans une table SQLite et que, dans le même temps, SQLite vérifie les contraintes d'unicité sur la même table. On pourrait alors rajouter la contrainte d'unicité lors de la création la table raw_data mais cela créerait une erreur qui empêchera le reste des données de s'enregistrer en base.
* Le choix de coupler la requête INSERT OR IGNORE avec SELECT constitue un choix pragmatique en l'absence de tests de performance. Nous discuterons ce point dans les améliorations possibles à la fin de ce rapport.


##### Sur l'ensemble du modèle analytique

La contrainte d'unicité s'applique également par "construction" à l'ensemble du modèle analytique. 

* Lors de la création du modèle analytique l'ensemble des tables de dimensions sont construites afin de représenter un index des valeurs possibles pour chaque dimension. Pour s'assurer aucune valeur n'est insérée dans les tables de dimensions, on ajoute la contrainte DISTINCT à nos requêtes d'insertion. Voici un exemple de requête d'insertion issue du fichier insertion.sql:
Néanmoins, le modèle analytique est plus permissif que le modèle transactionnel en terme d'enregistrement, on peut renseigner une table de dimensions avec une nouvelle valeur:
* on peut, par exemple, renseigner une nouvelle station sans pour autant avoir une mesure faisant référence à cette station.  

Ainsi, nous avons choisi de rajouter la contrainte d'unicité sur les tables de dimensions pour s'assurer que les tables restent conforment au modèle analytique:  

* un utilisateur qui décide de rajouter une nouvelle valeur dans une table de dimensions ne pourra le faire que si la valeur ajoutée est bien une nouvelle valeur.

### L'enrichissement des données

#### L'utilisation de Nominatim

De part son intégration à Python et les limitations juridiques imposées par l'API de google, Nominatim semble le choix le plus judicieux pour récupérer des données supplémentaires à partir des données de notre base.  

* Une première requête permet de récupérer les latitudes et longitudes de chaque station.

```
SELECT nom_station, latitude, longitude FROM station WHERE adresse IS NULL;
```

* on ne sélectionne que les stations qui n'ont pas encore bénéficié de l'enrichissement pour éviter des requêtes inutiles à Nominatim,
* Chaque couple de coordonnées est soumis à Nominatim qui renvoie un dictionnaire dont l'une des clés contient le dictionnaire des données administratives:  

```python
{'road': 'Rue de Maubec', 'town': 'Voiron',
 'municipality': 'Grenoble', 'county': 'Isère', 
 'state': 'Auvergne-Rhône-Alpes', 'country': 'France',
 'postcode': '38500', 'country_code': 'fr'}
```  

* Pour éviter d'enrichir la base de données avec des données redondantes nous n'avons garder que l'information de la rue,
* De plus, les noms de clés peuvent différer en fonction des coordonnées, néanmoins la clé 'road' est toujours présente.

Une fois la valeur de la clé 'road' est récupérée, elle est insérée dans le champ de la table "station" restée vide si l'enrichissement n'a pas déjà eu lieu. Voici la requête qui illustre la complémentarité entre Python et SQLite dans l'enrichissement des données:

```python
      cur.execute("""UPDATE station SET adresse = ? WHERE
      nom_station LIKE ?;""", (adresse,station_name))
```

#### Le pipeline de la donnée

Nous pouvons résumer les étapes du traitement de la donnée grâce au schéma suivant:  
![Pipeline de la donnée](./pipeline.png)
Nous sommes donc en présence d'un pipeline de type ELTL. A l'issue du pipeline, la base de données a été enrichie de toutes les données nécessaires à la visualisation des données.

## Les visualisations

### La cartographie

Folium semble être un choix judicieux:  

* il ne présente pas de dépendances à d'autres librairies Python contrairement aux autres solutions évoquées. Cela permettra une installation rapide sur le matériel des techniciens. 
* la possibilité de générer des fichiers HTML à partir des cartographies générées permettra d'accéder aux cartes d'une manière simple et durable. Une fois générée, la carte est consultable depuis le stockage présent sur le matériel du technicien.  

Comme évoqué dans l'état de l'art, la génération d'une carte avec Foium est intuitive. Elle doit suivre les étapes suivantes:  

* La création de la carte en elle-même qui fait appel à l'API OpenStreetMap,
* L'appel à la méthode .plugins.MarkerCluster qui permet de mettre en place le regroupement des points sur la carte en fonction du niveau de zoom. Cela permet une meilleure lisibilité et un supplément d'information sur l'importance d'une zone de la carte.
* On interroge la table station pour récupérer le nom et les coordonnées de chaque station.
* On insère le nom et les coordonnées dans un nouveau marqueur.
* On crée une mise en forme pour les popups lié à chaque marqueur, où figurent le nom de la station,
* On ajoute un tooltip qui permet d'afficher les informations enrichies provenant de notre base. Au sein de l'étiquette figure un lien vers une visualisation des relevés de pollution pour la station.  

Voici une illustration du résultat obtenu:  

![Capture d'écran de cartographie](cartographie.png)

La carte ainsi générée est sauvegardée en tant que fichier HTML qui pourra être consultée depuis un navigateur sans besoin d'une connexion.
Le tooltip intégré à chaque marqueur intègre un lien qui pointe vers le fichier de visualisation des relevés de polluants.

### Les graphiques

La création des marqueurs de station de la carte déclenche la génération des visualisations de relevés sur les deux dernières semaines. Pour se faire, nous avons choisi Plotly. La librairie semble être le meilleur complément à la cartographie car comme elle permet de générer rapidement des fichiers HTML, consultables depuis l'ordinateur où l'application a été lancée. La création des visualisations s'appuie sur des requêtes simples, ré-exploitées en Python pour générer les nouvelles requêtes qui, à leur tour, permettront de renseigner les informations nécessaires à Plotly. Voici les étapes en question:  

* pour chaque station, on récupère l'ensemble des polluants associées via une requête SQLite,
* on boucle en Python sur chaque valeur de polluant, cette valeur est insérée dans une requête SQLite qui renvoie l'ensemble des mesures sur les deux dernières semaines,
* le résultat de cette requête est scindé en 2 listes, une pour les dates, l'autre pour les valeurs correspondantes,
* ces deux listes sont alors insérées comme nouveau tracé dans au graphique correspondant à la station.
On obtient alors autant de graphiques que de stations, consultables directement depuis le répertoire crée à cet effet où accessibles depuis la cartographie. En voici un exemple:
![Représentation graphique de relevés](./graphique.png)

## La création de fichiers JSON

L'étape de création des graphiques a soulevé une question. Les informations nécessaires pour générer les graphiques ne sont pas disponibles en tant que telle en dehors d'une lecture graphique. Il est donc intéressant de conserver ces informations "en dur" dans un fichier de relevé propre à chaque station. Le choix a donc été fait de reprendre les étapes de la création des graphiques pour insérer les informations dans un fichier JSON. Son schéma est illustré par l'extrait suivant:

![extrait_JSON](./JSON.png)


# Retour d'expériences

## Les difficultés rencontrées

### la compréhension des fichiers disponibles

Les données issues de la base demandent une étude "manuelle" des données:  

* comprendre la structure globale des fichiers JSON: à quoi correspond un enregistrement?
* identifier les champs essentielles et ceux redondants issus d'un formatage particuliers comme c'est le cas avec les données de géolocalisation des fichiers JSON
* prendre en compte les champs dont l'importance ne peut pas être évalués comme "statut_validité": la plupart du temps, ce champ est vide mais le fait qu'il contienne une valeur peut indiquer que cette valeur est importante.
Il faut donc réaliser le bon arbitrage en identifiant les données redondantes qu'il est inutile de garder tout en gardent les valeurs manquantes qui représentent tout de même une information.

### Concevoir la base de données

Là encore, il faut réaliser un arbitrage entre une base de données qui soit adaptée au besoin:  

* identifier les étapes essentielles dans la construction et la mise à jour de la base de données,
* sans réaliser des étapes inutiles,
* mais anticiper les besoins futures pour créer une base données qui puissent évoluer et rester conforme aux exigences: il faut par exemple anticiper le fait qu'un utilisateur puisse interagir avec la base de données. Il faut permettre à un utilisateur d'intervenir lors du chargement des données ou lui permettre  d'alimenter le modèle analytique, sans pour autant remettre en cause le bon fonctionnement du pipeline de la données.

### Concevoir l'application

Ces réflexions sur la conception de la base de données peuvent s'appliquer à un plus haut niveau, dans la conception de l'application dans son ensemble. Imaginer une application répondant strictement aux besoins de l'utilisateur mais dont la structure permet son évolution demande la prise en compte de nombreux paramètres.

### La manipulation des fichiers JSON et CSV 

Les commandes pour manipuler un fichier CSV en Python requièrent que l'on se penche sur la documentation officielle:  

* la méthode .reader de la librairie csv renvoie un objet qui représente le document dans son ensemble,
* la fonction next permet de passer à la ligne suivante, mais si l'on applique cette fonction à un objet de type reader, next va lire la première ligne du document car aucune ligne n'a encore été lue.  

La même réflexion peut s'appliquer à la manipulation des fichiers JSON.

* Le "format" JSON demande quelques traitements préalables à sa création.  
* Après de nombreuses recherche, nous avons choisi de coupler l'utilisation de la libraire JSON pour manipuler les fichiers JSON avec la librairie CSV pour écrire les dictionnaires sous forme de texte.   


```python
a = []

    if not os.path.isfile(fname):
        a.append(dict_met)

        with open(fname, mode='w') as f:
            # indent is for whitespace between 2 dictionnary insertions
            f.write(json.dumps(a, indent=2))
    else:
        with open(fname) as feedsjson:
            feeds = json.load(feedsjson)

        feeds.append(dict_met)
        with open(fname, mode='w') as f:
            f.write(json.dumps(feeds, indent=2)) 
```  

En analysant le code ci-dessus:  

* le fichier doit au moins contenir un liste vide,
* les dictionnaires peuvent alors être insérer dans la liste,
* avant d'être écrite dans un fichier texte, la liste doit être encodée au format JSON avec la méthode .dump,
* si le fichier contient déjà de l'encodage JSON, il faut d'abord récupérer la liste décodée avec la méthode .load.  

La création des fichiers JSON demande donc d'être particulièrement méthodique. C'est la même approche qui a été retenue lors de la création des fichiers de relevés.


## Ce qu'il resterait à faire

### Mettre en place un environnement de test

La question de la mise en place d'un environnement de test s'est posé à mesure que les scripts de requêtes à la base de données étaient conçus. Un environnement de test comprendrait:  

* une fixture qui reproduirait une base de données fictive,
* des tests unitaires, qui se présentent généralement sous la forme de fonctions qui appelleraient les fonctions utilisées dans l'application. Les tests unitaires permettent donc de vérifier le comportement d'une fonction en l'utilisant dans un nouveau cadre.

La mise en place des environnements de tests rallonge le cycle de développement car il faut idéalement réaliser un test à chaque nouvelle création de fonctions. Toutefois, sur plus long terme, les tests garantissent:  

* la conformité du code vis-à-vis des spécifications,
* facilite la détection des erreurs de code.

La décision de ne pas mettre en place de tests unitaires a été motivé par le délai de mis à disposition de l'application relativement court. Si le projet devait demander plus de temps, la mise en place d'un environnement de test aurait été la première amélioration à apporter à l'application.  

### Une opportunité de tester la performance des contraintes d'unicité

Il pourrait être intéressant de répliquer la base de données pour mesurer l'impact des requêtes INSERT OR IGNORE et INSERT DISTINCT sur des jeux de données pensés pour l'occasion.

* L'idée serait alors de comparer le fait d'utiliser les 2 requêtes sur un jeu ne présentant très peu de doublons. On peut intuiter que la requête INSERT DISTINCT sera plus coûteuse car le moteur doit tout de même vérifier que l'ensemble des valeurs à insérer sont distinctes. De plus, lors de l'insertion dans la table qui présente la contrainte UNIQUE un nouveau contrôle doit être réalisé à chaque insertion.
* A l'opposé, un jeu de données ne présentant que des doublons demande moins de traitement à la requête INSERT DISTINCT car la valeur à garder en mémoire est unique. On devrait observer un gain de temps supplémentaire lors de l'insertion dans la table ayant la contrainte UNIQUE, car une seule valeur doit être comparée. La requête INSERT OR IGNORE perd de son intérêt car il faut tout de même vérifier l'unicité de chaque enregistrement pour chaque valeur déjà présente de la table de destination.  

Pour confirmer notre intuition, il faudra s'appuyer sur les fonctionnalités de SQLite: 

* Avec l'option .timer ON, il est possible de mesurer le temps pris par un script pour s'exécuter.
* La commande QUERY PLAN permet d'obtenir le détail des opérations réalisées par le moteur lors de l'exécution d'une requête.


# Références

* Sur l'implémentation du pipeline
https://opensource.com/article/19/4/bash-vs-python

* La documentation officielle de la libraire CSV
https://docs.python.org/3/library/csv.html

* Un tutoriel pour la prise en main de la librairie CSV
https://www.kite.com/python/answers/how-to-skip-the-first-line-of-a-csv-file-in-python

* La documentation officielle de la libraire JSON:
https://docs.python.org/fr/3/library/json.html

* Un tutoriel pour la prise en main de la librairie JSON
https://www.eduba.school/blog/travailler-avec-les-donnees-json-en-python

* Un tour d'horizon des librairies de visualisation sur Python
https://analyticsinsights.io/visualisation-des-donnees-python/

* Le site officiel de la libriaire Plotly
https://plotly.com/python/creating-and-updating-figures/

* Pour restaurer un backup de base de données SQLite:
https://gitlab.com/simplon-dev-data/grenoble-2020-2021/prairie/-/tree/master/SQLite

* Pour mettre les informations critiques en tant que variable d'environnement 
https://pypi.org/project/python-dotenv/


* La documentation officielle le geocodin
https://nominatim.org/release-docs/latest/admin/Installation/

* Un comparatif des technologies PostGRES et SQLIte
https://hevodata.com/learn/sqlite-vs-postgresql/

* Une présentation du framework d'automatisation Prefect
https://blog.ippon.fr/2020/06/08/prefect-workflow-automation-part1/


  

