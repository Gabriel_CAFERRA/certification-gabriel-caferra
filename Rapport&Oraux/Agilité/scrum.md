# La gestion de projet

## Le contexte

"Simplon souhaite obtenir un système permettant de récupérer et d'analyser automatiquement les offres d'emploi dans le secteur de la Data, avec un tableau de bord présentant des métriques métier intéressantes sous la forme d'une page web accessible en ligne"

## Pourquoi la méthode Scrum Agile

Au sein de notre équipe composé de 4 personnes nous avons décidé de mettre en place la méthode Agile:

* Pour des personnes qui n'ont pas l'habitude de travailler ensemble, cette méthode collaborative permet l'adoption de méthodes et d'un référentiel communs, 
* Idéale pour des échanges réguliers avec un client non technique, qui évalue difficilement le périmètre technique de son besoin.

### Des recommendations

La méthode Scrum est un ensemble de recommendations: 

* chaque équipe applique les recommendations qui lui semblent pertinentes,
* adapte les protocoles à ses besoins,

La méthode Scrum doit être un facteur d'épanouissement au sein d'un projet et non une contrainte supplémentaire.

### Une méthode pour communiquer

La méthodologie Agile s'adapte particulièrement aux échanges entre un client non-technique et une équipe de développeur. Les différentes cérémonies organisées sont un moyen de faire le trait d'union entre avancées techniques et un livrable évaluable par  par le client.

### La production d'artefacts

La méthodologie Scrum implique la définition d'artefact qui sont des preuves du "contrat" passé entre le client et l'éqauipe technique.
La méthode Scrum permet donc la transparence et la visibilité pour les deux parties à travers les artefacts produits

### Le principe d'itération

Les sprints sont des itérations qui permettent de s'accorder sur des livrables intermédiaires, les sprint goals, qui permettent:

* de raffiner la compréhension du besoin client,
* permettre une ré-orientation rapide du projet, le cas_échéant,

Ainsi, la création de valeur peut-être rapidement évaluée par le client et valider (ou non) les efforts de l'équipe technique.


## Les différents acteurs de la méthodologie agile Scrum, la Scrum Team

Le client en lui-même ne fait pas partie de l'équipe Scrum. Il peut être un client externe non technique, un autre service au sein de l'entreprise mais éventuellement une entité abstraite permettant le dialogue au sein de l'équipe.



### Le Scrum Master

Le Scrum Master est le maître de cérémonie de la méthodologie Scrum: 

* il organise et préside les cérémonies SCRUM en veillant au respect du protocole,

Il est également la personne bienveillante de l'équipe:

* il conseille les membres de l'équipe sur les difficultés qu'ils peuvent rencontrées et veille à ce que chacun s'épanouisse au sein du projet,
* il est chargé de faciliter la communication au sein des membres de l'équipe, en les mettant en relation ou en servant de médiateur.

### Le Product Owner

Le Product Owner est l'interlocuteur privilégié du client, à ce titre, il doit:

* identifier et valider le besoin initial avec le client,

Le Product Owner sert d'intermédiaire entre le client et l'équipe technique, pour ce faire, il doit:

* décomposer le besoin initial par thèmes, les epic,
* décomposer chaque epic en besoins utilisateurs, appelés User Stories, dans le but de facilité leur traduction technique,
* regrouper ces User Stories par thème au sein d'Epic, le cas échéant,
* il anticipe les objectifs d'avancement du produit sur une ou plusieurs itérations, en tenant compte de la priorisation des User stories.

Il élaborer un premier artefact, le Product Backlog, qui présente à l'équipe technique le résultat des échanges avec le client.

### Les développeurs

Les développeurs sont les référents techniques du projet à ce titre, ils sont chargés d'identifier les besoins utilisateurs réalisables au cours du sprint, pour ce faire:

* ils réalisent la traduction technique des besoins utilisateurs, sur la base du Product Baclog,

* ils identifient les tâches techniques nécessaires à l'aboutissement d'un sprint, en accord avec le Product Owner,


Ces étapes doivent aboutir à la production d'un deuxième artefact, le sprint backlog où figurent les User stories retenues.

Sur la durée d'un sprint, ils doivent:

* réaliser les tâches, 
* faire part de leur avancement quotidiennement lors des cérémonies Daily Scrum


## Les artefacts de la méthode SCRUM

Peut-être intégrer ça dans la section product owner par exemple

### Le Product Backlog

Lors de l'élaboration du Product Backlog, le Product owner peut réaliser une première étape de décomposition des epic:

![Préliminaire backlog](./BacklogPreliminaire.png)

Le Product Backlog regroupe l'ensemble des User Stories, une bonne pratique consiste à organiser les user stories en fonction de leur priorité et d'avoir une référence à l'epic auxquelles elles se rattachent.
Voici une proposition de Product Backlog réalisé lors du projet en partenariat avec Simplon:

![Product Backlog](./sprint_3_priorité_backlog.png)

Le premier Product Backlog sert de référence tout au long du projet cependant, il peut évoluer au fur et à mesure des sprints quand le product owner identifie de nouveaux besoins utilisateurs. 

### Le Sprint Backlog

Pour faciliter la réalisation du Sprint Backlog, il peut être intessant de réaliser document permettant de valider les tâches techniques à conserver lors du sprint:
![Aide au sprint](./sprint_1_backlog.png)

Le Sprint Backlog est donc l'ensemble des User Stories retenues pour le sprint, le sprint goal étant de valider cet ensemble. Si ce n'est pas le cas, l'évaluation des tâches techniques est à revoir.


### Le livrable

Le dernier artefact est le produit en cours de conception. Il doit être conforme aux exigences du sprint backlog et doit être présenté au client lors de la cérémonie dédiée.

## Les cérémonies des Sprint

Voici les cérémonies dans leur ordre chronologique:

### Le sprint planning

Le sprint planning est la cérémonie préliminaire au début d'un Sprint. Il est essentiel que l'équipe Scrum au complet soit présente lors de cette cérémonie car:

* le Product Owner propose au reste de l’équipe l'objectif atteignable à l'issue du sprint qui démarre,
* l'ensemble de l'équipe passent en revue et valident les user stories qui correspondent à cette proposition,
* chaque user story est décomposée en tâches techniques, généralement appelées "issue".

Remarques:
Il est essentiel que la décomposition en tâches techniques soit la plus précise possible afin de s'assurer l'atteinte des objectifs du sprint. Pour aider l'équipe à réaliser cette estimation, on peut organiser un poker planning:

* il permet d'obtenir une estimation relative l'ensemble des compétences et des niveaux de chacun des membres.
* l'unité de mesure restera la même tout au long des sprints seul leur équivalence en temps pourra être ajustée.
* en même temps, chaque développpeur propose une estimation de la difficulté de la user story grâce à un système de points.
* cela permet un nouvel échange sur les difficultés identifiées par chacun,
* on réalise un dexuième tour d'estimation et calcule la moyenne arithmétique des valeurs. 
* cette moyenne constitue les story points attribués à chaque User story.

### Le daily scrum

Le daily scrum ou melee est un échange quotidien présidé par le Scrum Master où chaque développeur:

* fait part de son avancement pour la journée précédente,
* évoque les obstacles rencontrés,
* propose son objectif pour la journée,
* anticipe les nouveaux obstacles potentielles.
Cette réunion a pour objectif de retirer tout les freins qui pourraient entraver l'atteinte du sprint goal.

Remarque:
La réunion peut s'appuyer sur deux supports mis à jour par le Scrum Master, qui permettent de dynamiser l'intervention de chacun en l'axant sur l'objectif commun:

![scrumboard](./scrum_board.png)

* On vérifie que le scrum board est bien à jour avec les restitutions individuelles

![burndwonchart](./burndownchart.jpg)

* L'estimation du reste à faire de chaque développeur est ajouté et reporté sur le burndown chart.

### La sprint review

La sprint review est la cérémonie Scrum qui récapitule les avancées du Sprint. Le Scrum Master et le Product Owner se reunissent pour déterminer ses intervenants en gardant à l'esprit que:

* la sprint review doit se focaliser sur la valeur commerciale en l'état du projet,
* orientée autour d'une démonstration au client, en s'appuyant sur
* l'état actuel d'avancement du projet; ce qui a été fait pendant le sprint et ce qui est en cours de réalisation,
* c'est l'occasion pour le product owner de récolter les commentaires du client, raffiner le besoin utilisateur et d'adapter le product backlog en conséquence. 

### La sprint retrospective

La sprint retrospective est la cérémonie qui clôt le sprint en cours, c'est l'occasion pour l'ensemble de l'équipe de faire l'inspection du sprint, afin:

* d'inspecter les points positifs,
* d'identifier les difficultés rencontrées,
A l'issue de la cérémonie, l'équipe Scrum a identifié les axes d'amélioration organisés au sein d'un plan d'action. Il sera mis en place lors de la prochaine itération. 

Remarque:

L'équipe peut s'appuyer sur le burndown chart finalisé et le scrum board.

## On récapitule

![Cycle_Agile](./CycleAgile.png)
