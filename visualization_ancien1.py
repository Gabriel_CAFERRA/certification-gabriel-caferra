import sqlite3
from pathlib import Path
import folium
from folium import plugins
import plotly.graph_objects as go


def cartography(db_path, save_path):


    con = sqlite3.connect(db_path)
    cur = con.cursor()
    
    # filling each station key with polluting new keys

    cur.execute(
    """
    SELECT station."id_station",
    AVG(mesure.valeur), 
    unite, 
    polluant."nom_poll",
    station."nom_station", 
    station."longitude", 
    station."latitude", 
    station."adresse"
    FROM mesure
    JOIN polluant on polluant."id_polluant" = mesure."reference_polluant"
    JOIN station on station."id_station" = mesure."reference_station"
    AND datetime(mesure."date_debut") <= datetime('now') 
    AND datetime(mesure."date_debut") >= datetime('now', '-1 month')
    GROUP BY station."id_station"
    """)
    avg_pol = cur.fetchall()
    con.commit()
    
    # setting up folium cartography

    coordinates_origin = (46.227638, 2.213749)
    map = folium.Map(location=coordinates_origin, zoom_start=5)
    mc = folium.plugins.MarkerCluster(
                    name="Pollution atmosphérique en Rhône-Alpes-Auvergne",
                    location=coordinates_origin,
                    ).add_to(map)
    folium.LayerControl().add_to(map)

    for elt in avg_pol:

        id_station, tx_pol, unite, nom_pol, nom_station, longitude, latitude, adresse = elt

        # formatting string for HTML popups
        pol_str = f"CONCENTRATION MENSUELLE en {nom_pol}: {round(tx_pol,2)} {unite}"
        adress_str = f"ADRESSE: {adresse}"
        graph_link = pol_evolution(id_station, nom_station, db_path, save_path)
        # <ul style="width:max-content">
        # </ul>
        fol_pop = folium.Popup(f"""
                    <li style="width:max-content">{pol_str} <a href="{graph_link}"> historique des relevés</a> </li>
                    <li style="width:max-content">{adress_str}</li>
                """)

        folium.Marker(
            location=(latitude, longitude),
            tooltip=nom_station,
            popup=fol_pop
                ).add_to(mc)

    map.save('pol.html')  # Saves Map to an HTML File

def pol_evolution(id_station, nom_station ,db_path, save_path):

    #, detect_types=sqlite3.PARSE_DECLTYPES
    con = sqlite3.connect(db_path)
    cur = con.cursor()

    cur.execute(
        """
        SELECT date_debut, valeur
        FROM mesure
        JOIN station ON station."id_station" = mesure."reference_station"
        WHERE station."id_station" = ?
        ORDER BY datetime(date_debut);
        """,(id_station,))
    pol_evo = cur.fetchall()
    #print(pol_evo)
    con.commit()

    #fig = go.Figure()
    x_list, y_list = [], []
    for point in pol_evo:
        x_list.append(point[0]), y_list.append(point[1])
    
    #fig.add_trace(go.Scatter(x=x_list, y=y_list,
     #               mode='lines',
      #              name='pollution sur 2 semaines'))
    
    fig = go.Figure(
    data=[go.Bar(x=x_list, y=y_list)],
    layout=dict(title=dict(text="A Figure Specified By A Graph Object"))
)
    #fig = go.Figure()
    #fig.add_trace(go.Scatter((x = x_list, y=y_list, name='lines')))
    #fig=go.Figure(data=go.Bar(x=x_list, y=y_list))
    
    if '/' in nom_station:
        nom_station = nom_station.replace('/','-sur-')
    
    saved_fig_path = f"{save_path}/{nom_station}.html"
    
    fig.write_html(Path(saved_fig_path))
    return saved_fig_path