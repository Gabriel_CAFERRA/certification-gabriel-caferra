from loggin_config import logging_conf
from pathlib import Path
from csvconv import csv_converter
from dbload import csv_load, sql_script_execution
from enrichment import address_enrichment
from visualization import cartography
from db_backup import backup_creation
from dict_generation import dict_creation
import time

pol_dict={
"no2": "https://opendata.arcgis.com/datasets/08264f6574e844b8800ee0e18ca5ff9e_1.geojson",
"pm10": "https://opendata.arcgis.com/datasets/08264f6574e844b8800ee0e18ca5ff9e_4.geojson",
"pm25": "https://opendata.arcgis.com/datasets/08264f6574e844b8800ee0e18ca5ff9e_2.geojson",
"o3": "https://opendata.arcgis.com/datasets/08264f6574e844b8800ee0e18ca5ff9e_3.geojson",
"so2": "https://opendata.arcgis.com/datasets/08264f6574e844b8800ee0e18ca5ff9e_5.geojson",

}

db_file = "./pol_db.db"
backup_file = "dump.sql"
structure_script_path = "./structure.sql"
insertion_script_path = "./insertion.sql" 
backup_repo = "./back_up/"
logfile_path = "./logfile.log"
graphs_repo = "./graphs/"
dict_repo = "./releves/"
meta_data_path = "./back_up/geojson_metadata.json"

logging = logging_conf(logfile_path)

backup_path = Path(backup_repo)
if not backup_path.exists():
    backup_path.mkdir()
    logging.info(f"created backup directory: {backup_path}")

db_path = Path(db_file)
if not db_path.exists():
    logging.info(f"database created at: {db_path}")
else:
    logging.info(f"starting {db_path} update")

sql_script_execution(structure_script_path, db_path)



csv_load(
    csv_converter(pol_dict,backup_repo, meta_data_path),
    db_path)
logging.info(f"csvs loaded at {db_path}")

start = time.time()
sql_script_execution(insertion_script_path, db_path)

end = time.time()
time_taken = round(float(end-start), 1)
ms_taken = str(time_taken)
logging.info(
        f"{ms_taken}s to insert data in analytical model")
logging.info(f"database {db_path} updated")

address_enrichment(db_path)
logging.info(f"station table enriched with available address fields")

graphs_path = Path(graphs_repo)

if not graphs_path.exists():
    graphs_path.mkdir()
    logging.info(f"created graphs directory: {graphs_path}")

cartography(db_path, graphs_path)
logging.info(f"generated vizualisations")

dump_path = backup_creation(db_path, backup_repo, backup_file)
logging.info(f"backup done at: {dump_path}")

dict_path = Path(dict_repo)
if not dict_path.exists():
    dict_path.mkdir()
    logging.info(f"created dictionary directory: {dict_path}")
dict_creation(db_path, dict_path)