import json
import csv
import requests
import logging
from datetime import datetime
import os

csv_col = [
    "nom_dept",
    "nom_com",
    "insee_com",
    "nom_station",
    "code_station",
    "typologie",
    "influence",
    "nom_poll",
    "id_poll_ue",
    "valeur",
    "unite",
    "metrique",
    "date_debut",
    "date_fin",
    "statut_valid",
    "x_wgs84",
    "y_wgs84",
    "x_reglementaire",
    "y_reglementaire",
    "OBJECTID",
]

def is_in_met(fname, key, date, hour):
    
    if os.path.isfile(fname):
        with open(fname) as feedsjson:
            feeds = json.load(feedsjson)
        
        for elt in reversed(feeds):

            if elt['polluant'] == key and elt['heure'] == hour and elt['date'] == date:
                logging.info(f"last {key} measures where already downloaded ")
                return True
            
        return False
    else:
        
        return False

def metadata_edition(dict_met,fname):
    
    a = []

    if not os.path.isfile(fname):
        a.append(dict_met)

        with open(fname, mode='w') as f:
            # indent is for whitespace between 2 dictionnary insertions
            f.write(json.dumps(a, indent=2))
    else:
        with open(fname) as feedsjson:
            feeds = json.load(feedsjson)

        feeds.append(dict_met)
        with open(fname, mode='w') as f:
            f.write(json.dumps(feeds, indent=2))

def csv_converter(pol_dict, save_path, fname):


    csv_file_list = []
    
    for key in pol_dict:
        
        # setting save name and path
        day, time, hour = datetime.today().strftime("%Y-%d-%m"), datetime.now().strftime("%H-%M-%S"), datetime.now().strftime("%H")
        

        if not is_in_met(fname, key, day ,hour):
             

            pol_url= pol_dict[key]
            
            # downloading file as response object, transforming it as JSON dict and selecting relevant key
            
            geo_json = requests.get(pol_url).json()

            

            lines_num = len(geo_json["features"])
            col_names = list(geo_json["features"][1]["properties"].keys())
            dict_met = {'polluant': key, 'date': day, 'heure': hour, 'nbre_lignes': lines_num, 'nbre_colonnes': col_names, 'url': pol_url}

            if "features" not in geo_json.keys():
                logging.error(f'"features" field missing in {pol_url}')
                continue
            
            geo_dict = geo_json["features"] 
            
            # maybe use Path lib
            csv_path = f"{save_path}{day}_{time}_{key}.csv"  

            with open(csv_path, 'a', newline='') as csv_file:
                writer = csv.DictWriter(csv_file, fieldnames=csv_col)
                writer.writeheader()

                for count, elt in enumerate(geo_dict, start=1):
                    
                    # checking that each record has the relevant fields
                    if list(elt["properties"].keys()) != csv_col:
                        logging.warning(f"non matching fields at record {count} in {pol_url}")
                        continue

                    line = {field: elt["properties"][field] for field in csv_col}
                    writer.writerow(line)
                
                logging.info(f"{csv_path} created")

            csv_file_list.append(csv_path)
    
            metadata_edition(dict_met, fname)           
    
    return csv_file_list
