import sqlite3
import os
import csv
import logging
import json
from pathlib import Path

def sql_script_execution(sql_script,db_path):
    conn = sqlite3.connect(db_path)
    cur = conn.cursor()

    # running sql script for raw and clean table creation
    with open(sql_script, 'r') as sql_file:
        sql_for_execution = sql_file.read()
    cur.executescript(sql_for_execution)
    conn.commit()

    cur.close()
    conn.close()


def metadata_control(file,csv_header):
    
    with open(file) as csv_file:
        rows = csv.reader(csv_file)
        
        header = next(rows)
        
        if header != csv_header:
            logging.error(f'unsuitable fields in {csv_file}')
            return False         
            
    return file



def csv_load(file_list, db_path):
    
    csv_header = [
            "nom_dept","nom_com","insee_com","nom_station",
            "code_station","typologie","influence","nom_poll",
            "id_poll_ue","valeur","unite","metrique",
            "date_debut","date_fin","statut_valid","x_wgs84",
            "y_wgs84","x_reglementaire","y_reglementaire","OBJECTID"]
    
    

    for file in file_list:

        
        validated_file = metadata_control(file, csv_header)

        if not validated_file:
            continue

        conn = sqlite3.connect(db_path)
        cur = conn.cursor()
        cur.execute("DELETE FROM raw_data;")

        with open(validated_file) as csv_file:

            rows = csv.reader(csv_file)
            header = next(rows)

            # each csv file is loaded in raw_data
            cur.executemany("INSERT INTO raw_data VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)",rows)
            conn.commit()
            # 
            cur.execute("""
            INSERT OR IGNORE INTO clean_data
            SELECT *
            FROM raw_data
            ;
            """)
            conn.commit()
        # ON CONFLICT (nom_station, nom_poll, valeur, date_debut, date_fin, x_wgs84, y_wgs84) DO NOTHING
            cur.close()
            conn.close()
