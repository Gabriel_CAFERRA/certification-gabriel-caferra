import sqlite3
import os
from geopy.geocoders import Nominatim
from dotenv import dotenv_values

def address_enrichment(db_path):

    config = dotenv_values(".env")
    
    geolocator = Nominatim(user_agent=config['UA_CERTIF'])

    con = sqlite3.connect(db_path)
    cur = con.cursor()
 
    cur.execute("""SELECT nom_station, latitude, longitude FROM station WHERE adresse IS NULL;""")
    stations_to_enrich = cur.fetchall()

    con.commit()

    for station in stations_to_enrich:
        

        station_name, latitude, longitude = station[0], str(station[1]), str(station[2])
        
        ####### pas besoin de bloquer le téchargement ? 
        # Si envoyé un message mais le rajouter aussi dans le request
        
        location = geolocator.reverse(latitude+","+longitude, timeout=100000)
        
        if 'road' not in location.raw['address'].keys():
            adresse = "NULL"
        else:
            adresse = location.raw['address']['road']
            
        cur.execute("""
        UPDATE station 
        SET adresse = ? 
        WHERE nom_station LIKE ?;
        """, (adresse,station_name))
        
        con.commit()

    cur.close()
    con.close()
