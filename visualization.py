import sqlite3
from pathlib import Path
import folium
from folium import plugins
import plotly.graph_objects as go

from SQLqueries import station_ref, polluting_ref, pol_measures, erroneous_val

def cartography(db_path, save_path):

    # setting up folium cartography

    coordinates_origin = (46.227638, 2.213749)
    map = folium.Map(location=coordinates_origin, zoom_start=5)
    mc = folium.plugins.MarkerCluster(
                    name="Pollution atmosphérique en Rhône-Alpes-Auvergne",
                    location=coordinates_origin,
                    ).add_to(map)
    folium.LayerControl().add_to(map)

    ref_station = station_ref(db_path)

    for station in ref_station:
        
        (id_station, nom_station, longitude, latitude, adresse, commune, departement) = station

        graph_link = graph_edition(db_path, save_path, nom_station)

        folium.Marker(
            location=(latitude, longitude),
            tooltip=f"STATION {nom_station}",
            # call for 
            popup=f"""
                    <li style="width:max-content">RUE: {adresse}</li>
                    <li style="width:max-content">COMMUNE: {commune}</li>
                    <li style="width:max-content">DEPARTEMENT: {departement}</li>
                    <li style="width:max-content">ACCEDER à l'<a href="{graph_link}"> historique des relevés</a> </li>
                """).add_to(mc)

    map.save('pol.html')  # Saves Map to an HTML File


def graph_edition(db_path, save_path, nom_station):

    # setting up measures graphs
    fig = go.Figure()

    ref_poll = polluting_ref(db_path, nom_station)

    for polluant in ref_poll:

        (nom_polluant, id_polluant) = polluant
        
        ref_releves = pol_measures(db_path, nom_station, id_polluant)
    
        (val_manq,val_ab) = erroneous_val(db_path, nom_station, id_polluant)

        x_list, y_list = [], []
        for point in ref_releves:
            
            x_list.append(point[0]), y_list.append(point[1])
            
        fig.add_trace(go.Scatter(
            x = x_list, y=y_list, mode='lines', name= f"{nom_polluant}, manquantes:{val_manq}, aberrantes:{val_ab}" ))

        fig.update_layout(
            title=f"Niveaux de pollution relevés à {nom_station}",
            legend=dict(
            yanchor="top",
            y=-0.25,
            xanchor="left",
            x=0.01
            ))
        

    if '/' in nom_station:
        nom_station = nom_station.replace('/','-sur-')

    saved_fig_path = f"{save_path}/{nom_station}.html"
    fig.write_html(Path(saved_fig_path))

    return saved_fig_path
            

        