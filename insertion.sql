

INSERT OR IGNORE INTO departement ("nom_departement")
SELECT DISTINCT ("nom_dept") 
FROM clean_data;


INSERT OR IGNORE INTO commune ("nom_commune","insee_com")
SELECT DISTINCT ("nom_com"), "insee_com"
FROM clean_data;


INSERT OR IGNORE INTO station ("nom_station","longitude", "latitude")
SELECT DISTINCT ("nom_station"), "x_wgs84", "y_wgs84"
FROM clean_data;


INSERT OR IGNORE INTO polluant ("nom_poll")
SELECT DISTINCT ("nom_poll") 
FROM clean_data;


INSERT OR IGNORE INTO mesure ("valeur","reference_station","reference_polluant","reference_commune","reference_departement","date_debut","date_fin","unite","metrique")
SELECT valeur, station."id_station", polluant."id_polluant", commune."id_commune", departement."id_departement", date_debut, date_fin, unite, metrique 
FROM clean_data
JOIN station
ON station."nom_station" = clean_data."nom_station"
JOIN polluant
ON polluant."nom_poll" = clean_data."nom_poll"
JOIN commune
ON commune."nom_commune" = clean_data."nom_com"
JOIN departement
ON departement."nom_departement" = clean_data."nom_dept";