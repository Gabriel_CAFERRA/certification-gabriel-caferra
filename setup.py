from setuptools import setup

setup(name='pol_app',
    version='1.0',
    description='polluting app',
    url='https://gitlab.com/Gabriel_CAFERRA/certification-gabriel-caferra',
    author='Gabriel CAFERRA',
    author_email='gabriel.caferra.simplon@gmail.com',
    license='SIMPLON',
    packages=['pol_app'],
    entry_points = {
        'console_scripts': ['polluting-app=main'],
    },
    zip_safe=False)