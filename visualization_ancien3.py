import sqlite3
from pathlib import Path
import folium
from folium import plugins
import plotly.graph_objects as go

def cartography(db_path, save_path):

    # setting up folium cartography

    coordinates_origin = (46.227638, 2.213749)
    map = folium.Map(location=coordinates_origin, zoom_start=5)
    mc = folium.plugins.MarkerCluster(
                    name="Pollution atmosphérique en Rhône-Alpes-Auvergne",
                    location=coordinates_origin,
                    ).add_to(map)
    folium.LayerControl().add_to(map)

    con = sqlite3.connect(db_path)
    cur = con.cursor()
    
    # 
    cur.execute(
        """
        SELECT id_station, nom_station, longitude, latitude, adresse FROM station;
        """)
    
    ref_station = cur.fetchall()
    con.commit()

    for station in ref_station:
        
        id_station, nom_station, longitude, latitude, adresse = station

        folium.Marker(
            location=(latitude, longitude),
            tooltip=f"""
                    <li style="width:max-content">STATION: {nom_station} </li>
                    <li style="width:max-content">ADRESSE: {adresse}</li>
                """,
            # call for 
            popup=graph_edition(cur, save_path, nom_station)
                ).add_to(mc)

    map.save('pol.html')  # Saves Map to an HTML File

    cur.close()
    con.close()

def graph_edition(cur, save_path, nom_station):

    # setting up measures graphs
    fig = go.Figure()

    cur.execute(
    """
    SELECT DISTINCT(nom_poll), id_polluant FROM polluant
    JOIN mesure on polluant."id_polluant" = mesure."reference_polluant"
    JOIN station on station."id_station" = mesure."reference_station"
    WHERE station."nom_station" = ?;
    """,(nom_station,))

    ref_poll = cur.fetchall()

    for polluant in ref_poll:

        (nom_polluant, id_polluant) = polluant

        cur.execute(
            """
            SELECT date_debut, valeur as val, unite
            FROM mesure
            JOIN polluant on polluant."id_polluant" = mesure."reference_polluant"
            JOIN station on station."id_station" = mesure."reference_station"
            AND datetime(mesure."date_debut") <= datetime('now') 
            AND datetime(mesure."date_debut") >= datetime('now', '-1 month')
            WHERE station."nom_station" = ? AND polluant."id_polluant" = ?;
            """,(nom_station,id_polluant))

        ref_releves = cur.fetchall()
        print(ref_releves)
        x_list, y_list = [], []
        for point in ref_releves:
            x_list.append(point[0]), y_list.append(point[1])

        fig.add_trace(go.Scatter(
            x = x_list, y=y_list, mode='lines'))

    if '/' in nom_station:
        nom_station = nom_station.replace('/','-sur-')

    saved_fig_path = f"{save_path}/{nom_station}.html"
    fig.write_html(Path(saved_fig_path))

    return saved_fig_path
