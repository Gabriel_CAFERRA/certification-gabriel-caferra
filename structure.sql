PRAGMA foreign_keys = ON;

CREATE TABLE IF NOT EXISTS raw_data (
            nom_dept TEXT,
            nom_com TEXT,
            insee_com TEXT,
            nom_station TEXT,
            code_station TEXT,
            typologie TEXT,
            influence TEXT,
            nom_poll TEXT,
            id_poll_ue TEXT,
            valeur NUMERIC NULL,
            unite TEXT,
            metrique TEXT,
            date_debut TEXT,
            date_fin TEXT,
            statut_valid BOOLEAN,            
            x_wgs84 FLOAT,
            y_wgs84 FLOAT,
            x_reglementaire FLOAT,
            y_reglementaire FLOAT,
            OBJECTID INTEGER
            );

CREATE TABLE IF NOT EXISTS clean_data (
            nom_dept TEXT,
            nom_com TEXT,
            insee_com TEXT,
            nom_station TEXT,
            code_station TEXT,
            typologie TEXT,
            influence TEXT,
            nom_poll TEXT,
            id_poll_ue TEXT,
            valeur NUMERIC NULL,
            unite TEXT,
            metrique TEXT,
            date_debut TEXT,
            date_fin TEXT,
            statut_valid BOOLEAN,            
            x_wgs84 FLOAT,
            y_wgs84 FLOAT,
            x_reglementaire FLOAT,
            y_reglementaire FLOAT,
            OBJECTID INTEGER,
            UNIQUE (nom_station, nom_poll, valeur, date_debut, date_fin, x_wgs84, y_wgs84)
            );


CREATE TABLE IF NOT EXISTS departement (
    id_departement INTEGER NOT NULL,
    nom_departement TEXT,
    PRIMARY KEY (id_departement),
    UNIQUE (nom_departement)
    );

CREATE TABLE IF NOT EXISTS commune (
    id_commune INTEGER NOT NULL,
    nom_commune TEXT,
    insee_com TEXT,
    PRIMARY KEY (id_commune),
    UNIQUE (nom_commune, insee_com)
    );

CREATE TABLE IF NOT EXISTS station (
    id_station INTEGER NOT NULL,
    nom_station TEXT,
    longitude FLOAT,
    latitude FLOAT,
    adresse TEXT,
    PRIMARY KEY (id_station),
    UNIQUE (nom_station, longitude, latitude)
    );

CREATE TABLE IF NOT EXISTS polluant (
    id_polluant INTEGER NOT NULL,
    nom_poll TEXT,
    PRIMARY KEY (id_polluant),
    UNIQUE (nom_poll)
    );

CREATE TABLE IF NOT EXISTS mesure (
    id_mesure INTEGER NOT NULL,
    valeur NUMERIC NULL,
    reference_station INT,
    reference_polluant INT,
    reference_commune INT,
    reference_departement INT,
    date_debut TEXT,
    date_fin TEXT,
    unite TEXT,
    metrique TEXT,
    PRIMARY KEY (id_mesure),
    FOREIGN KEY (reference_station) REFERENCES station ("id_station"),
    FOREIGN KEY (reference_polluant) REFERENCES polluant ("id_polluant"),
    FOREIGN KEY (reference_commune) REFERENCES commune ("id_commune"),
    FOREIGN KEY (reference_departement) REFERENCES departement ("id_departement"),
    UNIQUE (valeur, reference_station, reference_polluant, date_debut, date_fin)
    );