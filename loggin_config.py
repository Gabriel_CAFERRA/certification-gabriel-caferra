import logging

def logging_conf(logfile_path):

    logging.basicConfig(level=logging.INFO,
                        format="[%(levelname)s] %(message)s",
                        handlers=[
                        logging.FileHandler(logfile_path),
                        logging.StreamHandler()
                                ]
                        )

    logging.basicConfig(level=logging.WARNING,
                        format="[%(levelname)s] %(message)s",
                        handlers=[
                        logging.FileHandler(logfile_path),
                        logging.StreamHandler()
                                ]
                        )

    logging.basicConfig(level=logging.ERROR,
                        format="[%(levelname)s] %(message)s",
                        handlers=[
                        logging.FileHandler(logfile_path),
                        logging.StreamHandler()
                                ]
                        )
    
    return logging