from pathlib import Path
from SQLqueries import station_ref, polluting_ref, pol_measures, erroneous_val
import json
import os

def dict_creation(db_path,save_path):

    ref_stat = station_ref(db_path)

    dict_station = dict.fromkeys([elt[1] for elt in ref_stat])

    for station in ref_stat:
        
        a = []

        id_station, nom_station, longitude, latitude = station[0:4]
        
        if '/' in nom_station:
            nom_station = nom_station.replace('/','-sur-')
        
        if ' ' in nom_station:
            nom_station = nom_station.replace(' ','_')

        fname=f"{save_path}/{nom_station}.json"
        if os.path.isfile(fname):
            os.remove(fname)

        ref_poll=polluting_ref(db_path,nom_station)
        

        pol_list = []
        for polluant in ref_poll:

            (nom_polluant, id_polluant,) = polluant

            ref_releves = pol_measures(db_path, nom_station,id_polluant)
            
            err_val = erroneous_val(db_path, nom_station, id_polluant)

            pol_dict = dict.fromkeys(['nom_polluant','releve','dates','unite','val. manq.', 'val. aber.'])
            pol_dict['nom_polluant']= nom_polluant
            
            pol_dict['releve']= [elt[1] for elt in ref_releves]
            pol_dict['dates']= [elt[0] for elt in ref_releves]
            pol_dict['unite'] = ref_releves[0][2]
            pol_dict['val. manq.']= err_val[0]
            
            pol_dict['val. aber.']= err_val[1]
            
            
            a.append(pol_dict)

            with open(fname, mode='w') as f:
                # indent is for whitespace between 2 dictionnary insertions
                f.write(json.dumps(a, indent=2))
            """
            a =[]
            if not os.path.isfile(fname):
                a.append(pol_dict)

                with open(fname, mode='w') as f:
                    # indent is for whitespace between 2 dictionnary insertions
                    f.write(json.dumps(a, indent=2))
            else:
                with open(fname) as feedsjson:
                    feeds = json.load(feedsjson)

                feeds.append(pol_dict)
                with open(fname, mode='w') as f:
                    f.write(json.dumps(feeds, indent=2))
            """