import sqlite3

def station_ref(db_path):
    
    con = sqlite3.connect(db_path)
    cur = con.cursor()
    
    # 
    cur.execute(
        """
        SELECT id_station, nom_station, longitude, latitude, adresse, commune."nom_commune", departement."nom_departement"
        FROM station
        JOIN commune ON commune."id_commune" = mesure."reference_commune"
        JOIN departement ON departement."id_departement" = mesure."reference_departement"
        JOIN mesure ON station."id_station" = mesure."reference_station"
        GROUP BY station."id_station"
        ;
        """)
    
    ref_station = cur.fetchall()
    con.commit()

    cur.close()
    con.close()

    return ref_station


def polluting_ref(db_path,nom_station):

    con = sqlite3.connect(db_path)
    cur = con.cursor()

    cur.execute(
    """
    SELECT DISTINCT(nom_poll), id_polluant FROM polluant
    JOIN mesure on polluant."id_polluant" = mesure."reference_polluant"
    JOIN station on station."id_station" = mesure."reference_station"
    WHERE station."nom_station" = ?;
    """,(nom_station,))

    ref_poll = cur.fetchall()
    con.commit()

    cur.close()
    con.close()

    return ref_poll

def pol_measures(db_path, nom_station,id_polluant):

    con = sqlite3.connect(db_path)
    cur = con.cursor()

    cur.execute(
        """
        SELECT date_debut, valeur, unite
        FROM mesure
        JOIN polluant on polluant."id_polluant" = mesure."reference_polluant"
        JOIN station on station."id_station" = mesure."reference_station"
        AND datetime(mesure."date_debut") <= datetime('now') 
        AND datetime(mesure."date_debut") >= datetime('now', '-1 month')
        WHERE station."nom_station" = ? AND polluant."id_polluant" = ?
        ORDER BY date_debut;
        """,(nom_station,id_polluant))

    ref_releves = cur.fetchall()
    con.commit()

    cur.close()
    con.close()

    return ref_releves

def erroneous_val(db_path, nom_station,id_polluant):

    con = sqlite3.connect(db_path)
    cur = con.cursor()

    cur.execute(
        """
        SELECT SUM(valeur = '') as val_manq, SUM(valeur < 0) as val_neg
        FROM mesure
        JOIN polluant on polluant."id_polluant" = mesure."reference_polluant"
        JOIN station on station."id_station" = mesure."reference_station"
        AND datetime(mesure."date_debut") <= datetime('now') 
        AND datetime(mesure."date_debut") >= datetime('now', '-1 month')
        WHERE station."nom_station" = ? AND polluant."id_polluant" = ?;
        """,(nom_station,id_polluant))
    (val_manq,val_ab) = cur.fetchone()
    con.commit()

    cur.close()
    con.close()

    return (val_manq,val_ab)